# coding:utf-8
'''
@file: 用某个特定值来切分字符串.py
@author: jingsheng hong
@ide: PyCharm
@createTime: 2020年08月20日  15点02分
@contactInformation: 727803257@qq.com
'''

# 请从这行开始编写脚本

c = '//www.hxmec.com/static/info-details/info-detail-913.html'

c = c.split("//")
print(c)
print(c[1])


a = '『平价供应·抗疫联合公益行动』 "全国紧缺医用物资供需平台”全面上线'
print(a)
if '"' in a:
    b = a
    b = b.replace('"','/"',1000).replace('“',"/“",10000).replace('”',"/”",10000)
print(b)


vv = '『平价供应·抗疫联合公益行动』 "全国紧缺医用物资供需平台”全面上线'

d = '""1"'
print(d)