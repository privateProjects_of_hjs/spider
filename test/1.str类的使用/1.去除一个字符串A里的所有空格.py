# coding:utf-8
'''
@file: 1.去除一个字符串A里的所有空格.py
@author: jingsheng hong
@ide: PyCharm
@createTime: 2020年08月14日  16点48分
@contactInformation: 727803257@qq.com
'''

# 请从这行开始编写脚本

a = "1994 / 美国 / 犯罪 剧情"
a = list(a)
print(a)  # 打印出来，就能看到每个下标对应的值了

# b = []
# print(a)
# for  i in a :
#     if i == " " or i =="\xa0":
#         pass
#     else:
#         b.append(i)
# print(b)
# c = ''.join(b)
# print(c)

d = "1994 / 美国 / 犯罪 剧情"
d=d.replace("\xa0","")   # 删除一个字符串A里的类似空格的“\xa0”这些所有的值
print(d)

e = "1994 / 美国 / 犯罪 剧情"
e=e.replace(" ","")   # 删除一个字符串A里的所有空格的值
print(e)

f = "1994 / 美国 / 犯罪 剧情"
f = f.replace("\xa0","").replace(" ","")   # 删除一个字符串A里的类似空格的“\xa0”这些所有的值,也删除一个字符串A里的所有空格的值
print(f)

h = "1994 / 美国 / 犯罪 剧情"
h = h.replace("\xa0","").replace('\n', '').replace('\r', '')   # 去掉一个字符串A里所有的回车符和换行符，也删除一个字符串A里的类似空格的“\xa0”这些所有的值
print(h)


# 情况字符串里面所有的空格

cc ='                                 8月12日，海西医药交易中心获得厦门市地方金融协会授牌，正式加入协会。    '
print(cc)
cc = cc.strip(" ")
print(cc)
d = ''.join(cc.split())
print(d)