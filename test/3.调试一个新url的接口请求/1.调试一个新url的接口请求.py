# coding:utf-8
'''
@file: 1.调试一个新url的接口请求.py
@author: jingsheng hong
@ide: PyCharm
@createTime: 2020年08月16日  12点03分
@contactInformation: 727803257@qq.com
'''

# 请从这行开始编写脚本



from fake_useragent import UserAgent
from lxml import etree

import requests
userAgent = UserAgent()
random_ua = userAgent.random
print(random_ua)
headers = {"User-Agent" : random_ua}

def demo1():
    '''验证百度首页请求'''
    result = requests.get(url="https://www.baidu.com",headers=headers)
    print(result.text)
    # 保存html返回值为html页面的操作；参考网址为：https://zhidao.baidu.com/question/1544460132295753587.html
    with  open("demo1.html","wb") as f:
        f.write(result.content)

# demo1()

def demo2():
    '''验证起点小说请求'''
    result = requests.get(url="https://www.qidian.com/rank/hotsales?stype=1&page=1",headers=headers)
    print(result.text)
    with  open("demo2.html","wb") as f:
        f.write(result.content)

# demo2()

def demo3():
    '''验证链家二手房请求'''
    result = requests.get(url="https://su.lianjia.com/ershoufang/",headers=headers)
    print(result.text)
    with  open("demo3.html","wb") as f:
        f.write(result.content)

#demo3()

def demo4():
    '''验证摄图网请求'''
    result = requests.get(url="http://699pic.com/photo-0-10-popular-all-0-all-all-1-0-0-0-0-0-0-all-all.html",headers=headers)
    print(result.text)
    with  open("demo4.html","wb") as f:
        f.write(result.content)

    html_selector = etree.parse("demo4.html",etree.HTMLParser())
    root = html_selector.xpath('//div[@class="list"][1]/a/@href')
    print(root)

demo4()