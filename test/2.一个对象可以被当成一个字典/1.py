# coding:utf-8
'''
@file: 1.py
@author: jingsheng hong
@ide: PyCharm
@createTime: 2020年08月15日  16点44分
@contactInformation: 727803257@qq.com
'''

# 请从这行开始编写脚本

class A:
	def __init__(self):
		self.a = 1
		self.b = 2


a = A()
print(a.__dict__)
# {'a': 1, 'b': 2}
#获取变量a：
print(a.__dict__['a'])
print(a["a"])