# coding:utf-8
'''
@file: 3.py
@author: jingsheng hong
@ide: PyCharm
@createTime: 2020年08月15日  16点45分
@contactInformation: 727803257@qq.com
'''

# 请从这行开始编写脚本


class A:

    aa = ""
    bb = ""
    def __init__(self):
        self.a = self.aa
        self.b = 2

    def keys(self):
        return ('a','b',)

    def __getitem__(self, item):
        return getattr(self, item)

a = A()

print(dict(a))
print(a["a"])
a.aa  =3
print(a["a"])

