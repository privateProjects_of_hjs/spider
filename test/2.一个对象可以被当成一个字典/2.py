# coding:utf-8
'''
@file: 2.py
@author: jingsheng hong
@ide: PyCharm
@createTime: 2020年08月15日  16点45分
@contactInformation: 727803257@qq.com
'''

# 请从这行开始编写脚本

class A:
	def __init__(self):
		self.a = 1
		self.b = 2

	def keys(self):
		return ('a','b',)

	def __getitem__(self, item):
		return getattr(self, item)

a = A()
print(dict(a))
print(a["a"])