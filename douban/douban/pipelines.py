# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
from .settings import mysql_database_connection_information
from  .utils.mysqlUtil import MysqlUtil
mysqlUtil = MysqlUtil()
from scrapy import Request

class DoubanPipeline:

    def __init__(self):
        self.connect = MysqlUtil()
        self.connect.mysql_database_connection_information = mysql_database_connection_information
        self.connect.mysql_truncate_data("movie_information",0)


    def process_item(self, item, spider):
        # 1.电影序号
        movie_serial_id = item['movie_serial_id']
        # 2.电影名
        movie_name = item['movie_name']
        # 3.电影出版信息
        movie_publish_information = item['movie_publish_information']
        # 4.电影星级
        movie_star = item['movie_star']
        # 5.电影评价人数
        number_of_movie_reviews = item['number_of_movie_reviews']
        # 6.电影描述
        movie_describe = item['movie_describe']

        # 7.电影封面图片地址
        picture_address_of_movie = item['picture_address_of_movie']

        # a = Request(url=picture_address_of_movie)
        #
        # with open(r"E:\spiderProjects\douban\douban\电影封面"+ r"\\" + movie_serial_id+ movie_name+".jpg",'wb') as f:
        #     f.write(a)
        #     f.close()


        sql = 'insert into movie_information(movie_serial_id,movie_name,movie_publish_information,movie_star,number_of_movie_reviews,movie_describe,picture_address_of_movie) VALUES ("{}","{}","{}","{}","{}","{}","{}")'.format(movie_serial_id,movie_name,movie_publish_information,movie_star,number_of_movie_reviews,movie_describe,picture_address_of_movie)
        self.connect.mysql_execute(sql)
        # 这句 return item  不能注释掉，因为注释掉的话会导致没返回item值，会导致相关日志打印不出来；
        # return 1

        return item