# Scrapy settings for douban project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://docs.scrapy.org/en/latest/topics/settings.html
#     https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://docs.scrapy.org/en/latest/topics/spider-middleware.html


#==>第一部分：基本配置<===

#1、项目名称，默认的USER_AGENT由它来构成，也作为日志记录的日志名
BOT_NAME = 'douban'

#2、爬虫应用路径
# 值用点隔开，表示下一级的意思；比如"douban.spiders",表示：根目录文件名douban.根目录的下一级文件名spiders；
# 路径的作用，框架会通过路径找到对应目录的所在绝对路径，可以在该目录下进行新增文件新增数据等操作；
SPIDER_MODULES = ['douban.spiders']
NEWSPIDER_MODULE = 'douban.spiders'


#3、客户端User-Agent请求头
# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'douban (+http://www.yourdomain.com)'
USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36"

#4、是否遵循爬虫协议
# Obey robots.txt rules
ROBOTSTXT_OBEY = False

#5、是否支持cookie，cookiejar进行操作cookie，默认开启
# Disable cookies (enabled by default)
#COOKIES_ENABLED = False

#6、Telnet用于查看当前爬虫的信息，操作爬虫等...使用telnet ip port ，然后通过命令操作
# Disable Telnet Console (enabled by default)
#TELNETCONSOLE_ENABLED = False


#7、Scrapy发送HTTP请求默认使用的请求头
# Override the default request headers:
#DEFAULT_REQUEST_HEADERS = {
#   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#   'Accept-Language': 'en',
#}



#===>第二部分：并发与延迟<===

#1、下载器总共最大处理的并发请求数,默认值16
# Configure maximum concurrent requests performed by Scrapy (default: 16)
#CONCURRENT_REQUESTS = 32

#2、每个域名能够被执行的最大并发请求数目，默认值8
#CONCURRENT_REQUESTS_PER_DOMAIN = 16


#3、能够被单个IP处理的并发请求数，默认值0，代表无限制，需要注意两点
#3.1、如果不为零，那CONCURRENT_REQUESTS_PER_DOMAIN将被忽略，即并发数的限制是按照每个IP来计算，而不是每个域名；
#3.2、该设置也影响DOWNLOAD_DELAY，如果该值不为零，那么DOWNLOAD_DELAY下载延迟是限制每个IP而不是每个域；
#CONCURRENT_REQUESTS_PER_IP = 16

#4、如果没有开启智能限速，这个值就代表一个规定死的值，代表对同一网址延迟请求的秒数（值的单位为秒）
# Configure a delay for requests for the same website (default: 0)
# See https://docs.scrapy.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
DOWNLOAD_DELAY = 0.5



#===>第五部分：中间件、Pipelines、扩展<===

# 1、Enable or disable spider middlewares
# See https://docs.scrapy.org/en/latest/topics/spider-middleware.html
#SPIDER_MIDDLEWARES = {
#    'douban.middlewares.DoubanSpiderMiddleware': 543,
#}

# 2、Enable or disable downloader middlewares
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
DOWNLOADER_MIDDLEWARES = {  # 注释表示不生效，没注释表示生效；
   # 'douban.middlewares.DoubanDownloaderMiddleware': 543,    # 这个是系统自带的中间件（这个中间件不能实现ip代理/cookie代理/user-agent代理/这三个里的其中一个功能）
   'douban.middlewares.UserAgentDownloaderMiddleware': 545, # 这是我新增的中间件，实现随机获取一个user-agent值的功能
   #'douban.middlewares.ProxyDownloaderMiddleware': 544,           # 这是我新增的中间件，实现ip代理的功能
}


# 3、Enable or disable extensions
# See https://docs.scrapy.org/en/latest/topics/extensions.html
#EXTENSIONS = {
#    'scrapy.extensions.telnet.TelnetConsole': None,
#}

# 4、Configure item pipelines
# See https://docs.scrapy.org/en/latest/topics/item-pipeline.html
ITEM_PIPELINES = { # 必须开启这个，开启之后才能执行这个类，并执行类中自己编写的存储数据的代码块把数据存储到数据表；
   'douban.pipelines.DoubanPipeline': 300,
}




#===>第六部分：缓存<===
"""
1. 启用缓存
    目的用于将已经发送的请求或相应缓存下来，以便以后使用
    from scrapy.downloadermiddlewares.httpcache import HttpCacheMiddleware
    from scrapy.extensions.httpcache import DummyPolicy
    from scrapy.extensions.httpcache import FilesystemCacheStorage
"""

# 是否启用缓存策略
#HTTPCACHE_ENABLED = True

# 缓存超时时间
#HTTPCACHE_EXPIRATION_SECS = 0

# 缓存保存路径
#HTTPCACHE_DIR = 'httpcache'

# 缓存忽略的Http状态码
#HTTPCACHE_IGNORE_HTTP_CODES = []

# 缓存存储的插件
#HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'



# HTTPERROR_ALLOWED_CODES = [403]


# Enable and configure the AutoThrottle extension (disabled by default)
# See https://docs.scrapy.org/en/latest/topics/autothrottle.html
#AUTOTHROTTLE_ENABLED = True



# The initial download delay
#AUTOTHROTTLE_START_DELAY = 5


# The maximum download delay to be set in case of high latencies
#AUTOTHROTTLE_MAX_DELAY = 60


# The average number of requests Scrapy should be sending in parallel to
# each remote server
#AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0


# Enable showing throttling stats for every response received:
#AUTOTHROTTLE_DEBUG = False

# Enable and configure HTTP caching (disabled by default)
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings




#===>第七部分：自己结合实际功能需要，写的相关变量和对应变量值，(目的：把这些变量存储在该setting.py，供其他脚本调用拿去使用)<===


# 1.自己自定义这些数据库连接的基础属性和对应的基础属性值

mysql_database_connection_information = {
                                         "host":     "127.0.0.1",
                                         "port":      3306,
                                         "user":     "root",
                                         "passwd":   "123456",
                                         "db":        "douban",
                                         "charset":  "utf8"
                                         }


# 2.自己构建的一个IP代理池 （代理池其实就是一个有可用的IP和IP账号的列表）
# 相关教程可参考这两个网址：
# https://www.cnblogs.com/JinZL/p/11738020.html
# https://jianshu.com/p/e67065fb5e6a
PROXIES =[
    {'ip':'127.0.0.1:6379','pwd':'zwz:1234'},  # 有账号密码
    {'ip':'127.0.0.1:6372','pwd':None},         # 没有账号密码
    {'ip':'127.0.0.1:6373','pwd':None},
    {'ip':'127.0.0.1:6370','pwd':None}
]

# 3.自己构建的一个user-agent池 （user-agent池其实就是一个有多个可用user-agent的列表）--》目前采用直接调用fake_useragent库来读取随机正确的user-agent
# 相关教程可参考这两个网址：
# https://www.cnblogs.com/JinZL/p/11738020.html
# https://www.cnblogs.com/xieqiankun/p/know_middleware_of_scrapy_1.html
# USER_AGENT_LIST = [
#                       "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36",
#                       "Dalvik/1.6.0 (Linux; U; Android 4.2.1; 2013022 MIUI/JHACNBL30.0)",
#                       "Mozilla/5.0 (Linux; U; Android 4.4.2; zh-cn; HUAWEI MT7-TL00 Build/HuaweiMT7-TL00) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1",
#                       "Mozilla/5.0 (Linux; U; Android 4.1.1; zh-cn; HTC T528t Build/JRO03H) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30; 360browser(securitypay,securityinstalled); 360(android,uppayplugin); 360 Aphone Browser (2.0.4)",
#                 ]



# 3.添加这个变量的原因：scrapy用命令行scrapy crawl <spider> -o 文件名.json 输出文件时，会默认使用unicode编码，当内容为中文时，输出的json文件里就会把中文都转为非中文字符不方便使用人员查看数据；
# 做法：可以在setting.py文件中修改默认的输出编码方式：只需要在setting.py中增加如下语句；
# 参考学习网址：https://www.cnblogs.com/linkr/p/7995454.html
# 参考学习网址：https://blog.csdn.net/BlankGrid/article/details/104706760
FEED_EXPORT_ENCODING = 'utf-8'