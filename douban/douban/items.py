# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class DoubanItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass

    # 1.电影序号
    movie_serial_id = scrapy.Field()
    # 2.电影名
    movie_name = scrapy.Field()
    # 3.电影出版信息
    movie_publish_information = scrapy.Field()
    # 4.电影星级
    movie_star = scrapy.Field()
    # 5.电影评价人数
    number_of_movie_reviews = scrapy.Field()
    # 6.电影描述
    movie_describe = scrapy.Field()
    # 7.电影封面图片地址
    picture_address_of_movie = scrapy.Field()
