import scrapy
from ..items import DoubanItem
doubanItem = DoubanItem()

class DoubanSpiderSpider(scrapy.Spider):
    # spider名
    name = 'douban_spider'
    # 只允许爬取的域名（避免爬取到其他域名的数据）
    allowed_domains = ['movie.douban.com']
    # url入口
    start_urls = ['https://movie.douban.com/top250']
    # 用于拼接
    base_url = 'https://movie.douban.com/top250'

    def parse(self, response):
        pass
        movie_list = response.xpath('//div[@class="article"]/ol[@class="grid_view"]/li')
        for movie in movie_list:
            # 1.电影序号
            doubanItem["movie_serial_id"] =  movie.xpath('.//div[@class="pic"]/em/text()').get()
            # 2.电影名
            doubanItem["movie_name"] = movie.xpath('.//div[@class="hd"]/a[1]/span[1]/text()').get()
            # 3.电影出版信息
            movie_publish_information = movie.xpath('.//div[@class="bd"]/p[1]/text()').getall()[1]
            doubanItem["movie_publish_information"] = ''.join(movie_publish_information.split())
            # 4.电影星级
            doubanItem["movie_star"] = movie.xpath('.//div[@class="star"]/span[@class="rating_num"]/text()').get()
            # 5.电影评价人数
            doubanItem["number_of_movie_reviews"] = movie.xpath('.//div[@class="star"]/span[4]/text()').get()
            # 6.电影描述
            doubanItem["movie_describe"] = movie.xpath('.//p[@class="quote"]/span/text()').get()
        
            # 7.电影封面图片地址
            doubanItem["picture_address_of_movie"] = movie.xpath('.//div[@class="pic"]/a[1]/img[1]/@src').get()

            yield doubanItem
        # 获取后一页的href
        href_of_next_page = response.xpath('//div[@class="paginator"]/span[@class="next"]/a/@href').get()

        if href_of_next_page:
            url_of_next_page =  self.base_url + href_of_next_page
            yield scrapy.Request(url=url_of_next_page,callback=self.parse)

