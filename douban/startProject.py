# coding:utf-8
'''
@file: startProject.py
@author: jingsheng hong
@ide: PyCharm
@createTime: 2020年08月14日  15点39分
@contactInformation: 727803257@qq.com
'''

# 请从这行开始编写脚本

from scrapy import cmdline


# 1.通过执行脚本，把命令行写在代码块里面，来替代用命令行来执行命令：能实现快速执行整个scrapy项目；也能提高调试效率；
cmdline.execute("scrapy crawl douban_spider".split())

# 2.通过执行脚本，把命令行写在代码块里面，来替代用命令行来执行命令：能实现执行整个scrapy项目；也能提高调试效率；也能在项目根目录下生产一个json格式的数据结果的txt文件，
# cmdline.execute("scrapy crawl douban_spider -o movieInformation.json".split())

# 3.通过执行脚本，把命令行写在代码块里面，来替代用命令行来执行命令：能实现执行整个scrapy项目；也能提高调试效率；也能在项目根目录下生产一个csv格式的数据结果的csv文件，
# cmdline.execute("scrapy crawl douban_spider -o movieInformation.csv".split())
