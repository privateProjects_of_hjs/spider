# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class SuzhouLianjiaHomeItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass
    # 1.房屋名称： name
    name = scrapy.Field()

    # 2.房屋户型：type
    type = scrapy.Field()

    # 3.建筑面积：area
    area = scrapy.Field()

    # 4.房屋朝向： orientation
    orientation = scrapy.Field()

    # 5.装修情况：decoration
    decoration = scrapy.Field()

    # 6.有无电梯：elevator
    elevator = scrapy.Field()

    # 7.房屋总价：total_price
    total_price = scrapy.Field()

    # 8.房屋单价：unit_price
    unit_price = scrapy.Field()

    # 9.房屋封面图图片地址：cover_image_address
    cover_image_address = scrapy.Field()

    # 10.房屋产权：property_right
    property_right = scrapy.Field()