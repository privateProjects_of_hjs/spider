import scrapy
from ..items import SuzhouLianjiaHomeItem
item = SuzhouLianjiaHomeItem()

class SuzhouLianjiaHomeSpiderSpider(scrapy.Spider):
    name = 'suzhou_lianjia_home_spider'
    allowed_domains = ['su.lianjia.com']
    start_urls = ['https://su.lianjia.com/ershoufang/']
    base_url = 'https://su.lianjia.com/ershoufang/'
    num = 1

    def parse(self, response):
        houselist = response.xpath('//div[@class="leftContent"]/ul/li[@class="clear LOGCLICKDATA"]')
        for house in houselist:

            # 1.房屋名称： name
            name = house.xpath('.//div[@class="positionInfo"]/a[1]/text()').get()
            houseBaseInformation = house.xpath('.//div[@class="houseInfo"]/text()').get()
            houseBaseInformation = houseBaseInformation.split("|")
            # 3.建筑面积： area
            area  = houseBaseInformation[1].strip()
            # 4.房屋朝向： orientation
            orientation = houseBaseInformation[2].strip()
            # 5.装修情况：decoration
            decoration = houseBaseInformation[3].strip()
            # 7.房屋总价：total_price
            total_price = house.xpath('.//div[@class="totalPrice"]/span/text()').get()
            # 8.房屋单价：unit_price
            unit_price = house.xpath('.//div[@class="unitPrice"]/span/text()').get()
            # 9.房屋封面图图片地址：cover_image_address
            cover_image_address = house.xpath('.//img[@class="lj-lazy"]/@src').get()
            item["name"] = name
            item["area"] = area
            item["orientation"] = orientation
            item["decoration"] = decoration
            item["total_price"] = total_price
            item["unit_price"] = unit_price
            item["cover_image_address"] = cover_image_address
            # 房屋详情页地址
            detailed_page_address = house.xpath('.//div[@class="title"]/a[1]/@href').get()
            yield scrapy.Request(url=detailed_page_address,callback=self.detailed_page_parse,meta={"item":item})
        self.num = self.num + 1
        # 下一页的房屋请求地址
        next_page_url = self.base_url + "pg" + str(self.num)
        if self.num<=100:
            yield scrapy.Request(url=next_page_url,callback=self.parse)

    def detailed_page_parse(self, response):

        # 2.房屋户型：type
        type = response.xpath('//div[@class="base"]//ul/li[1]/text()').get()
        # 6.有无电梯：elevator
        elevator = response.xpath('//div[@class="base"]//ul/li[last()]/text()').get()
        # 10.房屋产权：property_right
        property_right = response.xpath('//div[@class="transaction"]//ul/li[last()-2]/span[2]/text()').get()
        item = response.meta["item"]
        item["type"] = type
        item["elevator"] = elevator
        item["property_right"] = property_right

        yield item