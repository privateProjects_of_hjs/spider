# Scrapy settings for suzhou_lianjia_home project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://docs.scrapy.org/en/latest/topics/settings.html
#     https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://docs.scrapy.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'suzhou_lianjia_home'

SPIDER_MODULES = ['suzhou_lianjia_home.spiders']
NEWSPIDER_MODULE = 'suzhou_lianjia_home.spiders'


# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'suzhou_lianjia_home (+http://www.yourdomain.com)'
USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36"

# Obey robots.txt rules
ROBOTSTXT_OBEY = False

# Configure maximum concurrent requests performed by Scrapy (default: 16)
#CONCURRENT_REQUESTS = 32

# Configure a delay for requests for the same website (default: 0)
# See https://docs.scrapy.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
DOWNLOAD_DELAY = 0.5
# The download delay setting will honor only one of:
#CONCURRENT_REQUESTS_PER_DOMAIN = 16
#CONCURRENT_REQUESTS_PER_IP = 16

# Disable cookies (enabled by default)
#COOKIES_ENABLED = False

# Disable Telnet Console (enabled by default)
#TELNETCONSOLE_ENABLED = False

# Override the default request headers:
#DEFAULT_REQUEST_HEADERS = {
#   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#   'Accept-Language': 'en',
#}

# Enable or disable spider middlewares
# See https://docs.scrapy.org/en/latest/topics/spider-middleware.html
#SPIDER_MIDDLEWARES = {
#    'suzhou_lianjia_home.middlewares.SuzhouLianjiaHomeSpiderMiddleware': 543,
#}

# Enable or disable downloader middlewares
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
DOWNLOADER_MIDDLEWARES = {
   # 'suzhou_lianjia_home.middlewares.SuzhouLianjiaHomeDownloaderMiddleware': 543,
   'suzhou_lianjia_home.middlewares.UserAgentDownloaderMiddleware': 545, # 这是我新增的中间件，实现随机获取一个user-agent值的功能
   #'suzhou_lianjia_home.middlewares.ProxyDownloaderMiddleware': 544,           # 这是我新增的中间件，实现ip代理的功能
}

# Enable or disable extensions
# See https://docs.scrapy.org/en/latest/topics/extensions.html
#EXTENSIONS = {
#    'scrapy.extensions.telnet.TelnetConsole': None,
#}

# Configure item pipelines
# See https://docs.scrapy.org/en/latest/topics/item-pipeline.html
ITEM_PIPELINES = {
   'suzhou_lianjia_home.pipelines.SuzhouLianjiaHomePipeline': 300,
   'suzhou_lianjia_home.pipelines.CSVPipeline': 400,
   'suzhou_lianjia_home.pipelines.SQLPipeline': 500,
}

# Enable and configure the AutoThrottle extension (disabled by default)
# See https://docs.scrapy.org/en/latest/topics/autothrottle.html
#AUTOTHROTTLE_ENABLED = True
# The initial download delay
#AUTOTHROTTLE_START_DELAY = 5
# The maximum download delay to be set in case of high latencies
#AUTOTHROTTLE_MAX_DELAY = 60
# The average number of requests Scrapy should be sending in parallel to
# each remote server
#AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0
# Enable showing throttling stats for every response received:
#AUTOTHROTTLE_DEBUG = False

# Enable and configure HTTP caching (disabled by default)
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
#HTTPCACHE_ENABLED = True
#HTTPCACHE_EXPIRATION_SECS = 0
#HTTPCACHE_DIR = 'httpcache'
#HTTPCACHE_IGNORE_HTTP_CODES = []
#HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'



#===>第七部分：自己结合实际功能需要，写的相关变量和对应变量值，(目的：把这些变量存储在该setting.py，供其他脚本调用拿去使用)<===


# 1.自己自定义这些数据库连接的基础属性和对应的基础属性值
# mysql_host = "127.0.0.1"
# mysql_port = "3306"
# mysql_db_name = "hongjingsheng"
# mysql_db_collection = "test1"
mysql_database_connection_information = {
                                         "host":     "127.0.0.1",
                                         "port":      3306,
                                         "user":     "root",
                                         "passwd":   "123456",
                                         "db":        "douban",
                                         "charset":  "utf8"
                                         }


# 2.自己构建的一个IP代理池 （代理池其实就是一个有可用的IP和IP账号的列表）
# 相关教程可参考这两个网址：
# https://www.cnblogs.com/JinZL/p/11738020.html
# https://jianshu.com/p/e67065fb5e6a
PROXIES =[
    {'ip':'127.0.0.1:6379','pwd':'zwz:1234'},  # 有账号密码
    {'ip':'127.0.0.1:6372','pwd':None},         # 没有账号密码
    {'ip':'127.0.0.1:6373','pwd':None},
    {'ip':'127.0.0.1:6370','pwd':None}
]

# 3.自己构建的一个user-agent池 （user-agent池其实就是一个有多个可用user-agent的列表）--》目前采用直接调用fake_useragent库来读取随机正确的user-agent
# 相关教程可参考这两个网址：
# https://www.cnblogs.com/JinZL/p/11738020.html
# https://www.cnblogs.com/xieqiankun/p/know_middleware_of_scrapy_1.html
# USER_AGENT_LIST = [
#                       "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36",
#                       "Dalvik/1.6.0 (Linux; U; Android 4.2.1; 2013022 MIUI/JHACNBL30.0)",
#                       "Mozilla/5.0 (Linux; U; Android 4.4.2; zh-cn; HUAWEI MT7-TL00 Build/HuaweiMT7-TL00) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1",
#                       "Mozilla/5.0 (Linux; U; Android 4.1.1; zh-cn; HTC T528t Build/JRO03H) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30; 360browser(securitypay,securityinstalled); 360(android,uppayplugin); 360 Aphone Browser (2.0.4)",
#                 ]



# 3.添加这个变量的原因：scrapy用命令行scrapy crawl <spider> -o 文件名.json 输出文件时，会默认使用unicode编码，当内容为中文时，输出的json文件里就会把中文都转为非中文字符不方便使用人员查看数据；
# 做法：可以在setting.py文件中修改默认的输出编码方式：只需要在setting.py中增加如下语句；
# 参考学习网址：https://www.cnblogs.com/linkr/p/7995454.html
# 参考学习网址：https://blog.csdn.net/BlankGrid/article/details/104706760
FEED_EXPORT_ENCODING = 'utf-8'