# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
import re
from scrapy.exceptions import DropItem

# 1.第一步，先执行这个管道A：这个管道用于数据的清理；（管道A里获取到的item值，是从spider传递过来的对象）
class SuzhouLianjiaHomePipeline:
    def process_item(self, item, spider):
        # 3.建筑面积 area
        item["area"] = re.findall("\d+\.?\d*",item["area"])[0]  # 只获取除了中文的数字和小数点
        # 8.房屋单价：unit_price
        item["unit_price"] = re.findall("\d+\.?\d*",item["unit_price"])[0]

        # 如果有不完全的数据，则抛弃
        if item["orientation"] == "暂无数据":
            raise DropItem("房屋朝向无数据，抛弃此项目:%s"%item)  # 主动抛出异常后，程序return值为null，管道A不会把这条项目传递给管道B，管道B就不会执行；

        return item

# 2.第二步，执行这个管道B：这个管道用于生成最终符合要求的csv文件（管道B里获取到的item值，是从管道A处理过后符合条件才传递给管道B的对象）
class CSVPipeline:
    file = None
    index = 0
    def open_spider(self,spider):  # 爬虫打开时，执行
        self.file = open("home.csv","a",encoding="utf-8")

    def process_item(self, item, spider):
        if self.index == 0:
            column_name = "name,type,area,orientation,decoration,elevator,total_price,unit_price,cover_image_address,property_right\n"
            self.file.write(column_name)
            self.index = 1
        home_str = item["name"]+","+item["type"]+","+item["area"]+","+item["orientation"]+","+item["decoration"]+","+item["elevator"]+","+item["total_price"]+","+item["unit_price"]+item["cover_image_address"]+item["property_right"]+"\n"
        self.file.write(home_str)

        return item

    def close_spider(self,spider): # 爬虫结束时，执行
        self.file.close()
# 3.第三步，执行这个管道C：这个管道用于持久化存储item数据到数据库（管道C里获取到的item值，是从管道B原封不动地传递给管道C的对象）
class SQLPipeline:
    # 相关代码块后续再写
    pass


# 细节： 管道A、管道B、管道C，这ABC的执行顺序一定要明确，可以是ABC，也可以是ACB；但管道A一定要先执行，因为对每次从spider传递过来的item对象里的值进行清理！
# 从类的角度来形象理解pipelines管道文件实现的功能： 在该脚本文件生成多个不同管道ABCDEFG等等这些不同的类，
    # 然后给这些类设置执行顺序，让item值在这些管道类按照执行顺序进行流转，在每个管道类里各自执行不同的功能，比如：清理数据、生成csv文件，保存数据到数据库等等操作！

