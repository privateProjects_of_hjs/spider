# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
from .settings import mysql_database_connection_information
from .utils.mysqlUtil import MysqlUtil


class DoubandemoPipeline:
    def __init__(self):
        self.connect = MysqlUtil()
        self.connect.mysql_database_connection_information = mysql_database_connection_information
        self.connect.mysql_truncate_data("test1",0)

    def process_item(self, item, spider):

        print(item)
        serial_number = item['serial_number']
        print(serial_number)
        movie_name = item['movie_name']
        # print(movie_name)
        introduce = item['introduce']
        # print(introduce)
        star = item['star']
        evaluate = item['evaluate']
        describes = item['describes']

        sql = 'insert into test1(serial_number,movie_name,introduce,star,evaluate,describes) VALUES ("{}","{}","{}","{}","{}","{}")'.format(serial_number,movie_name,introduce,star,evaluate,describes)
        self.connect.mysql_execute(sql)
        # 这句 return item  不能注释掉，因为注释掉的话会导致没返回item值，会导致相关日志打印不出来；
        # return 1

        return item

