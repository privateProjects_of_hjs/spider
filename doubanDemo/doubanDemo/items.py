# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class DoubandemoItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    # 序号
    serial_number = scrapy.Field()
    # 电影的名称
    movie_name = scrapy.Field()
    # 电影的介绍
    introduce = scrapy.Field()
    # 星级
    star = scrapy.Field()
    # 电影的评论数
    evaluate = scrapy.Field()
    # 电影的描述
    describes = scrapy.Field()
    a  = 111
if __name__ == "__main__":
    douban_item = DoubandemoItem()
    # douban_item.movie_name = 1
    # douban_item["a"] = 222
    douban_item["movie_name"] = 2
    douban_item["star"] = 1.2
    print(douban_item)
    # douban_item2 = DoubandemoItem(star ="PC",movie_name =100,price = 2)
    douban_item2 = DoubandemoItem(star ="PC",movie_name =100,)
    print(douban_item2)