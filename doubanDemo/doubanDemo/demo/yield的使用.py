# coding:utf-8
'''
@file: yield的使用.py
@author: jingsheng hong
@ide: PyCharm
@createTime: 2020年08月06日  09点26分
@contactInformation: 727803257@qq.com
'''

# 请从这行开始编写脚本


def foo():
    print("starting...")
    while True:
        res = yield 4
        print("res:",res)
g = foo()
print(next(g))
print("*"*20)

print(next(g))