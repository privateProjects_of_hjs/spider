# coding:utf-8
'''
@file: 验证callback和yield.py
@author: jingsheng hong
@ide: PyCharm
@createTime: 2020年08月08日  11点53分
@contactInformation: 727803257@qq.com
'''

# 请从这行开始编写脚本

# 请从这行开始编写脚本

class demo:

    def start_request(self):
        urls = ["a1" ,"b1","c1"]
        for i in urls:
            print(11)
            yield self.what(i,callback=self.parse)

    def what(self,Q):
        return Q

    def parse(self,p1):
        result = p1+"666"
        return result

if __name__ == "__main__":

    test = demo()
    a = test.start_request()
    print(a)
