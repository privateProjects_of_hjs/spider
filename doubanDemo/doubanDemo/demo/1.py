# coding:utf-8
'''
@file: 1.py
@author: jingsheng hong
@ide: PyCharm
@createTime: 2020年07月31日  23点48分
@contactInformation: 727803257@qq.com
'''

# 请从这行开始编写脚本

# for i in range(i):
#     n=qu[i]
#     url1 = "https://cq.lianjia.com/ershoufang/"+n+"/"
#     xiangying1 = requests.get(url1, headers=header).text
#     html = etree.HTML(xiangying1)
#     bankuai = html.xpath('//div[@data-role="ershoufang"]/div[2]/@href')
#     bankuai = [qu.replace('ershoufang', '') for qu in qu]
#     bankuai = [qu.replace('/', '') for qu in qu]
#     print(bankuai)

from lxml import etree
import requests
import json

r = requests.session()
headers = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36"}

baseUrl = "https://cq.lianjia.com/ershoufang/jiangbei"
response = r.get(baseUrl,headers=headers).text
html = etree.HTML(response)
div1 = html.xpath('//div[@data-role="ershoufang"]//div[1]//@href')  # div[1]前面有两个//，跟div[1]前面只有一个/，实现的效果是一样的；因为语法支持这两种语法；

newList = []
for i in div1:
     newI= i.replace("ershoufang","")
     newII = newI.replace("//","")
     newIII = newII.replace("/","")
     newList.append(newIII)
print(newList)


div2 = html.xpath('//div[@data-role="ershoufang"]/div[1]//@href')  # div[1]前面只有一个/

newList1 = []
for i in div2:
     I= i.replace("ershoufang","")
     II = I.replace("//","")
     III = II.replace("/","")
     newList1.append(III)
print(newList1)


# newDict = {}
# for m in newList:
#     url = "https://cq.lianjia.com/ershoufang/" + m
#     response = r.get(url,headers=headers).text
#     html = etree.HTML(response)
#     div2 = html.xpath('//div[@data-role="ershoufang"]/div[2]//@href')
#     list1 = []
#     for i in div2:
#         newI2 = i.replace("ershoufang","")
#         newII2 = newI2.replace("//","")
#         newIII2 = newII2.replace("/","")
#         list1.append(newIII2)
#     newDict[m] = list1
# print(newDict)
# jsons = json.dumps(newDict)
# print(jsons)