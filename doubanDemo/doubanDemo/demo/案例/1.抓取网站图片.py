# coding:utf-8
'''
@file: 1.抓取网站图片.py
@author: jingsheng hong
@ide: PyCharm
@createTime: 2020年08月12日  19点40分
@contactInformation: 727803257@qq.com
'''

# 请从这行开始编写脚本

import requests
import re
saveImagesPath = r"E:\douban\douban\demo\案例\图片集合"
headers = {
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36',
}
req = requests.get('https://www.kuaikanmanhua.com/web/comic/104457/',headers=headers)
req.encoding="unicode-escape"
j = 1
for i in re.findall('https://p1\.kkmh\.com/image/c104457/180820/.*?\.webp-w750\.jpg\?sign=.*?&t=.{8}',req.text):
    print(i)
    result = requests.get(url=i,headers=headers)

    with open(saveImagesPath+ r"\\" + str(j)+ ".jpg",'wb') as f:
        f.write(result.content)
    j =j+1