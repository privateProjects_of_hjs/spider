# coding:utf-8
'''
@file: 1.迭代器和生成器的学习.py
@author: jingsheng hong
@ide: PyCharm
@createTime: 2020年08月11日  11点03分
@contactInformation: 727803257@qq.com
'''

# 请从这行开始编写脚本

list = [1,2,3,4]
it = iter(list)
print(next(it))
print(next(it))
print(next(it))
print(next(it))

print("a"*20)
for i in list:
    print(i)

print("b"*20)
it1 = iter(list)
for j in it1:
    print(j)
