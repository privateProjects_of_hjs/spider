# coding:utf-8
'''
@file: 1.不带额外状态信息的回调函数.py
@author: jingsheng hong
@ide: PyCharm
@createTime: 2020年08月08日  16点05分
@contactInformation: 727803257@qq.com
'''

# 请从这行开始编写脚本


# 1.验证第一种场景
def my_callback(paramsList):
    '''
    :param paramsList 一个数据类型为list的数据
    '''
    print(*paramsList)

def caller(args666,functionName):
     '''
     :param args666         一个入参名，数据类型在这个函数里只能是list
     :param functionName    一个函数的函数名/一个回调函数的函数名
     '''
     functionName(args666)


my_callback(["1","2"])

caller([1,2],my_callback)


# 2.验证第二种场景


def my_callback(paramsList):
    '''
    :param paramsList 一个数据类型为list的数据
    '''
    print(*paramsList)

def caller(args666,callback):
     '''
     :param args666         一个入参名，数据类型在这个函数里只能是list
     :param callback    一个函数的函数名/一个回调函数的函数名
     '''
     callback(args666)


my_callback(["1","2"])

caller([1,2], callback=my_callback)


# 3.验证第三种场景 ： 发起调用的函数本身可以拥有多个被调用的回归函数所没有的入参


def my_callback(paramsList):
    '''
    :param paramsList 一个数据类型为list的数据
    '''
    print(*paramsList)

def caller(args666,callback ,otherOneParams):
     '''
     :param args666         一个入参名，数据类型在这个函数里只能是list
     :param callback    一个函数的函数名/一个回调函数的函数名
     '''
     callback(args666)


my_callback(["1","2"])

caller(args666=[1,2], callback=my_callback,otherOneParams=1)


# 4.验证第四种场景 ： 被调用的回归函数本身可以拥有多个发起调用的函数所没有的入参,同时也支持，发起调用的函数本身可以拥有多个被调用的回归函数所没有的入参

# 为什么可以这样的原因： 因为这两种函数都没限制入参个数，


def my_callback(paramsList, subOtherOneParams):
    '''
    :param paramsList 一个数据类型为list的数据
    '''
    print(*paramsList)
    print(subOtherOneParams)

def caller(args666 ,otherOneParams,callback):
     '''
     :param args666         一个入参名，数据类型在这个函数里只能是list
     :param callback    一个函数的函数名/一个回调函数的函数名
     '''
     callback(args666,otherOneParams)


import random
value = random.random()

caller(args666=[1,2],otherOneParams=value, callback=my_callback)





# 为了验证这行代码  scrapy.Request("http://movie.douban.com/top250" + next_link ,callback=self.parse )
# 所以 自己在本地脚本写的用于调试额的代码块：
import requests

def my_callback(response):
    '''
    :param response 一个结果
    '''
    print("这是最终得到的结果:")
    print(response)
def caller(url,callback):
     '''
     :param url         一个网站地址
     :param callback    一个函数的函数名/一个回调函数的函数名
     '''
     response = requests.get(url).content
     callback(response)


url = "https://wwww.baidu.com"
caller(url, callback=my_callback)



