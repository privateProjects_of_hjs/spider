# coding:utf-8
'''
@file: 2.带额外状态信息的回调函数.py
@author: jingsheng hong
@ide: PyCharm
@createTime: 2020年08月08日  17点57分
@contactInformation: 727803257@qq.com
'''

# 请从这行开始编写脚本


def apply_ascyn(func,args,callback,a):
    result = func(*args)
    callback(result)

def add(x,y):
    return x+y
def print_result(result,a):
    print(result)


apply_ascyn(add,[2,3],callback=print_result,a=666)
