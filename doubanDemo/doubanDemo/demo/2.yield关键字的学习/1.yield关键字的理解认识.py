# coding:utf-8
'''
@file: 1.yield关键字的理解认识.py
@author: jingsheng hong
@ide: PyCharm
@createTime: 2020年08月10日  16点58分
@contactInformation: 727803257@qq.com
'''

# 请从这行开始编写脚本

# 相关学习网网址：https://blog.csdn.net/mieleizhi0522/article/details/82142856/

def foo():
    pass
    print("starting...")
    while True:
        res = yield 4
        print("res值等于:",res)

g = foo()
# print(g)
print(next(g))
print("*"*20)
print(next(g))
print("*"*40)
print(next(g))
print("*"*60)
print(next(g))

print("h"*60)


def foo():
    print("starting...")
    while True:
        res = yield 4
        print("res:",res)
g = foo()
print(next(g))
print("*"*20)
print(g.send(7))

print("a"*60)

for n in range(10):
    a = n
    print(n)
print("b"*60)

def demo(num):
    print("starting...")
    while num<10:
        num = num+1
        yield num

for n in demo(7):
    print(n)

