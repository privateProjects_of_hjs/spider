# coding:utf-8
'''
@file: 2.yiled集合request请求的使用实战.py
@author: jingsheng hong
@ide: PyCharm
@createTime: 2020年08月11日  11点23分
@contactInformation: 727803257@qq.com
'''

# 请从这行开始编写脚本

import requests

def my_callback(response):
    '''
    :param response 一个结果
    '''
    print(response)
    print("这是最终得到的结果:")
    for j in range(0,3):
        print(j)
    yield caller("https://www.jianshu.com/",callback=my_callback)


def caller(url,callback):
     '''
     :param url         一个网站地址
     :param callback    一个函数的函数名/一个回调函数的函数名
     '''
     response = requests.get(url).content
     callback(response)


url = "https://wwww.baidu.com"
# caller(url, callback=my_callback)

# def demo():
#     print("111")
#     result = yield caller(url=url,callback=my_callback)
#     return result
#
# print(demo())
# next(demo())
# next(demo())
# print(next(demo()))

# def parse():
#     for aa in range(0,3):
#         yield  caller(url=url,callback=my_callback)
#
# print(parse())

def start_request():
    lists = ["https://wwww.baidu.com","https://blog.csdn.net/"]
    for i in lists:
        # print(i)
        yield caller(i,callback=my_callback)



next(start_request())
# next(start_request())
# next(start_request())

for i in range(0,3):
    yi