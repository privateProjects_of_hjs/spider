# coding:utf-8
'''
@file: 去除字符串中的逗号.py
@author: jingsheng hong
@ide: PyCharm
@createTime: 2020年08月06日  12点47分
@contactInformation: 727803257@qq.com
'''

# 请从这行开始编写脚本


a = "导演:路易·西霍尤斯LouiePsihoyos主演:RichardO'Barry/路易·西霍..."

if "'" in a:
    b = a.replace("'","")
    print(b)