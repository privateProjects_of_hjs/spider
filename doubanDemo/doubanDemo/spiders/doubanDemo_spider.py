import scrapy
from ..items import DoubandemoItem
doubandemoItem = DoubandemoItem()

class DoubandemoSpiderSpider(scrapy.Spider):
    name = 'doubanDemo_spider'
    allowed_domains = ['movie.douban.com']
    start_urls = ['http://movie.douban.com/top250']

    # 接口返回值都在这个方法里解析
    def parse(self, response):

        # 通过xpath语法，对接口返回值做html节点数据的处理
        # print(response.text)

        # 第一种xpath写法（简洁版的）
        # movie_list = response.xpath("//div[@class='article']//ol/li")
        # 第二种xpath写法（复杂版）（跟第一种xpath写法实现的效果是一样的）
        movie_list = response.xpath('//div[@id="content"]//div[@class="article"]/ol[@class="grid_view"]/li')
        # print(movie_list)
        for i_item in movie_list:
            # print(i_item)
            # douban_item["serial_number"] = i_item.xpath(".//div[@class='item']//em")
            # douban_item["serial_number"] = i_item.xpath(".//div[@class='item']//em/text()")
            # douban_item = DoubanItem()
            #douban_item["serial_number"] = i_item.xpath(".//div[@class='item']//em/text()").extract_first() # extract_first方法默认弃用，改用get方法替代；
            # douban_item["serial_number"] = i_item.xpath(".//div[@class='item']//em/text()").get()
            # douban_item["serial_number"] = i_item.xpath(".//div[@class='item']//em/text()").extract() # extract方法默认弃用，改用getall方法替代；
            # douban_item["serial_number"] = i_item.xpath(".//div[@class='item']//em/text()").getall()
            # print(douban_item)
            # 用于调试
            # completeXpath1= '//div[@id="content"]//div[@class="article"]/ol[@class="grid_view"]/li[1]//div[@class="pic"]/em/text()'
            # completeXpath2= '//div[@id="content"]//div[@class="article"]/ol[@class="grid_view"]/li[2]//div[@class="pic"]/em/text()'
            # completeXpath3= '//div[@id="content"]//div[@class="article"]/ol[@class="grid_view"]/li[3]//div[@class="pic"]/em/text()'
            # completeXpath4= '//div[@id="content"]//div[@class="article"]/ol[@class="grid_view"]/li[4]//div[@class="pic"]/em/text()'
            # completeXpath5= '//div[@id="content"]//div[@class="article"]/ol[@class="grid_view"]/li[5]//div[@class="pic"]/em/text()'
            # completeXpath6= '//div[@id="content"]//div[@class="article"]/ol[@class="grid_view"]/li[6]//div[@class="pic"]/em/text()'
            # completeXpath7= '//div[@id="content"]//div[@class="article"]/ol[@class="grid_view"]/li[7]//div[@class="pic"]/em/text()'
            # completeXpath8= '//div[@id="content"]//div[@class="article"]/ol[@class="grid_view"]/li[8]//div[@class="pic"]/em/text()'
            # completeXpath9= '//div[@id="content"]//div[@class="article"]/ol[@class="grid_view"]/li[9]//div[@class="pic"]/em/text()'
            # completeXpath10= '//div[@id="content"]//div[@class="article"]/ol[@class="grid_view"]/li[10]//div[@class="pic"]/em/text()'
            # completeXpath11= '//div[@id="content"]//div[@class="article"]/ol[@class="grid_view"]/li[11]//div[@class="pic"]/em/text()'
            # completeXpath12= '//div[@id="content"]//div[@class="article"]/ol[@class="grid_view"]/li[12]//div[@class="pic"]/em/text()'
            # completeXpath13= '//div[@id="content"]//div[@class="article"]/ol[@class="grid_view"]/li[13]//div[@class="pic"]/em/text()'
            # completeXpath14= '//div[@id="content"]//div[@class="article"]/ol[@class="grid_view"]/li[14]//div[@class="pic"]/em/text()'
            # completeXpath15= '//div[@id="content"]//div[@class="article"]/ol[@class="grid_view"]/li[15]//div[@class="pic"]/em/text()'
            # completeXpath16= '//div[@id="content"]//div[@class="article"]/ol[@class="grid_view"]/li[16]//div[@class="pic"]/em/text()'
            # completeXpath17= '//div[@id="content"]//div[@class="article"]/ol[@class="grid_view"]/li[17]//div[@class="pic"]/em/text()'
            # completeXpath18= '//div[@id="content"]//div[@class="article"]/ol[@class="grid_view"]/li[18]//div[@class="pic"]/em/text()'
            # completeXpath19= '//div[@id="content"]//div[@class="article"]/ol[@class="grid_view"]/li[19]//div[@class="pic"]/em/text()'
            # completeXpath20= '//div[@id="content"]//div[@class="article"]/ol[@class="grid_view"]/li[20]//div[@class="pic"]/em/text()'
            # completeXpath21= '//div[@id="content"]//div[@class="article"]/ol[@class="grid_view"]/li[21]//div[@class="pic"]/em/text()'
            # completeXpath22= '//div[@id="content"]//div[@class="article"]/ol[@class="grid_view"]/li[22]//div[@class="pic"]/em/text()'
            # completeXpath23= '//div[@id="content"]//div[@class="article"]/ol[@class="grid_view"]/li[23]//div[@class="pic"]/em/text()'
            # completeXpath24= '//div[@id="content"]//div[@class="article"]/ol[@class="grid_view"]/li[24]//div[@class="pic"]/em/text()'
            # completeXpath25= '//div[@id="content"]//div[@class="article"]/ol[@class="grid_view"]/li[25]//div[@class="pic"]/em/text()'

            # 1.序号
            doubandemoItem["serial_number"] = i_item.xpath(".//div[@class='item']//em/text()").get()
            # 2.电影的名称
            doubandemoItem["movie_name"] = i_item.xpath('.//div[@class ="hd"]/a/span[1]/text()').get()


            # 3.电影的介绍
            # douban_item["introduce"]  = i_item.xpath('.//div[@class="bd"]/p[1]/text()').getall() # 返回的是一个有两条数据的文本值的两条数据；
            # douban_item["introduce"]  = i_item.xpath('.//div[@class="bd"]/p[1]/text()').get() # 返回的是一个有两条数据的文本值的第一条数据；

            introduce = i_item.xpath('.//div[@class="bd"]/p[1]/text()').getall()
            # print(len(introduce))
            # print(introduce)
            empty_introduce = []
            for i in introduce:
                # print(i)
                new_i = "".join(i.split())
                if "'" in new_i:
                    new_i = new_i.replace("'","")
                    print(new_i)
                # print(new_i)
                empty_introduce.append(new_i)
            # 3.电影的介绍
            doubandemoItem["introduce"] = empty_introduce



            # 4.星级
            doubandemoItem["star"]  = i_item.xpath('.//div[@class="bd"]/div[@class="star"]//span[@class="rating_num"]/text()').get()
            # 5.电影的评论数
            doubandemoItem["evaluate"]  = i_item.xpath('.//div[@class="bd"]/div[@class="star"]//span[4]/text()').get()
            # 6.电影的描述
            doubandemoItem["describes"] = i_item.xpath('.//div[@class="bd"]/p[@class="quote"]/span/text()').get()



            # print(douban_item)
            yield doubandemoItem

        next_link = response.xpath('//div[@class="paginator"]/span[@class="next"]/link/@href').get()



        if next_link:
            yield scrapy.Request("http://movie.douban.com/top250" + next_link ,callback=self.parse )


        # yield scrapy.Request("http://movie.douban.com/top250"+next_link)



