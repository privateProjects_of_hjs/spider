# Define here the models for your spider middleware
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/spider-middleware.html

from scrapy import signals

# useful for handling different item types with a single interface
from itemadapter import is_item, ItemAdapter


class ToutiaoSpiderMiddleware:
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the spider middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    def process_spider_input(self, response, spider):
        # Called for each response that goes through the spider
        # middleware and into the spider.

        # Should return None or raise an exception.
        return None

    def process_spider_output(self, response, result, spider):
        # Called with the results returned from the Spider, after
        # it has processed the response.

        # Must return an iterable of Request, or item objects.
        for i in result:
            yield i

    def process_spider_exception(self, response, exception, spider):
        # Called when a spider or process_spider_input() method
        # (from other spider middleware) raises an exception.

        # Should return either None or an iterable of Request or item objects.
        pass

    def process_start_requests(self, start_requests, spider):
        # Called with the start requests of the spider, and works
        # similarly to the process_spider_output() method, except
        # that it doesn’t have a response associated.

        # Must return only requests (not items).
        for r in start_requests:
            yield r

    def spider_opened(self, spider):
        spider.logger.info('Spider opened: %s' % spider.name)






import time
from scrapy.http import HtmlResponse
from selenium.webdriver.common.by import By # BY模块
from selenium.webdriver.support.wait import WebDriverWait # 等待模块
from selenium.webdriver.support import expected_conditions as EC  # 预期条件的模块
from selenium.common.exceptions import TimeoutException,NoSuchElementException
class ToutiaoDownloaderMiddleware:
    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    def process_request(self, request, spider):
        # Called for each request that goes through the downloader
        # middleware.
        if spider.name == "douban_selenium_spider":
            # 用谷歌驱动器来打开指定网站
            spider.driver.get(request.url)
            # 把打开的谷歌浏览器的网站页面，最大化
            spider.driver.maximize_window()
            try:
                # 生成一个对象，设置最大等待时间为5秒
                wait = WebDriverWait(spider.driver,5)
                # wait.until(EC.presence_of_all_elements_located(By.XPATH('//div[@riot-tag="wcommonFeed"]')))
                # 执行js语句，把网页下拉滚动条往页面中间拉动1/2的屏幕
                spider.driver.execute_script('window.scrollTo(0,document.body.scrollHeight/2)')
                # 遍历2次，拉动滚动条使网页数据加载出2个页面的新内容
                for i in range(0,2):
                    # 每次遍历前，让程序暂停5秒让html网页元素数据都加载出来；
                    time.sleep(5)
                    spider.driver.execute_script('window.scrollTo(0,document.body.scrollHeight)')
                # 获取最终有包含所有豆瓣数据的html源码页面（该html页面已经经过js渲染了，js得到的数据已经渲染在该html页面）
                origin_code = spider.driver.page_source
                # 人为地构造一个response对象，在该response对象里对必要的几个入参各自赋值不同的入参值！
                res = HtmlResponse(url=request.url,encoding="utf-8",body=origin_code,request=request)
                time.sleep(2)
                # 关闭当前打开的浏览器，适当cpu内存；
                spider.driver.quit()
                # 返回的这个res变量，其实就是response对象，该对象会默认被引擎自动传递给spider类的parse解析方法里的入参response的入参值；
                return res
            except TimeoutException:
                print("time out !")
            except NoSuchElementException:
                print("no such element")
        return None

    def process_response(self, request, response, spider):
        # Called with the response returned from the downloader.

        # Must either;
        # - return a Response object
        # - return a Request object
        # - or raise IgnoreRequest
        return response

    def process_exception(self, request, exception, spider):
        # Called when a download handler or a process_request()
        # (from other downloader middleware) raises an exception.

        # Must either:
        # - return None: continue processing this exception
        # - return a Response object: stops process_exception() chain
        # - return a Request object: stops process_exception() chain
        pass

    def spider_opened(self, spider):
        spider.logger.info('Spider opened: %s' % spider.name)
