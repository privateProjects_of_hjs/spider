# coding:utf-8
"""
@file: common.py
@author: hjs
@ide: PyCharm
@createTime: 2020年08月14日  17点17分
@contactInformation: 727803257@qq.com
"""


# 请从这行开始编写脚本

class Common:

    @classmethod
    def delete_value_of_likeEmpty(cls, oneStr: str):
        """

        :param oneStr: 一个被处理的字符串
        :return: 一个被处理后的新字符串
        """
        oneStr = oneStr.replace("\xa0", "")
        return oneStr
