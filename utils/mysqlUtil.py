import pymysql
import random


# 在py3语言，用命令行“pip install pymysql”进行安装pymysql库；

# MySQLdb.connect()     # 建立数据库连接；
# cur = conn.cursor()   # 创建游标；
# cur.execute()         # 执行sql语句；
# cur.close()           # 关闭游标；
# conn.commit()         # 提交事物；（在向数据库新增/修改/删除一条数据时必须要有调用这个方法，否则sql语句不会被真正执行；)
# conn.rollback()       # 执行sql语句发生错误时候回滚；
# conn.close()          # 关闭数据库连接；(每次执行了sql语句，建议最后都要调用这个方法，因为这样可以降低内存/线程；)


class MysqlUtil:
    """
    1.该类的作用：实现连接mysql数据库，并执行相关sql操作；
    """

    # 属性含义：数据库连接信息；数据类型：dict；
    # 备注：该属性的属性值在子类里必须被重新赋值；

    mysql_database_connection_information = {

    }

    # 属性含义：表名；数据类型：str；
    # 备注：该属性的属性值在子子类里必须被重新赋值；
    tableName = "请必须输入一个数据表名，例如：tp_banner"

    # 属性含义：表注释；数据类型：str；
    # 备注：该属性的属性值在子子类里必须被重新赋值；
    tableComment = "请必须输入一个数据表的表注释，例如：仓库表"

    def __getConnect(self):
        '''
        1.该接口作用：返回一个数据库连接；
        '''
        try:
            conn = pymysql.connect(host=self.mysql_database_connection_information['host'],
                                   port=self.mysql_database_connection_information['port'],
                                   user=self.mysql_database_connection_information['user'],
                                   passwd=self.mysql_database_connection_information['passwd'],
                                   db=self.mysql_database_connection_information['db'],
                                   charset=self.mysql_database_connection_information['charset'])
            return conn

        except Exception as errorMessage:
            print("数据库连接异常,异常信息为：{errorMessage}".format(errorMessage=errorMessage))

    def __conn(self):
        '''
        1.该接口作用：返回一个数据库连接；
        '''
        conn = self.__getConnect()
        return conn

    def mysql_execute(self, sql_statement: str):
        '''
        :param  sql_statement；参数名：sql语句； 参数含义：新增/修改/删除语句；数据类型为：str；
        1.该接口作用：执行一条新增/修改/删除这3种类型的其中1种类型的sql语句；
        classTest.细节：执行新增/修改/删除这3种类型的sql语句的操作，必须有调用commit方法才能真正实现新增/修改/删除数据的功能；
                执行查询这1种类型的sql语句的操作，不需要调用commit方法也能实现查询数据的功能；
        '''
        # cur = self.__cur()
        # conn = self.__conn()
        # conn 和 cur这样设置才能生效
        conn = self.__getConnect()
        cur = conn.cursor()

        try:

            cur.execute(sql_statement)
        except Exception as errorMessage:
            # sql语句如果执行异常，会回滚；
            conn.rollback()
            print("数据库连接异常,异常信息为：{errorMessage}".format(errorMessage=errorMessage))

        else:
            cur.close()
            # sql语句如果执行正常，会提交；
            conn.commit()
            conn.close()

    def mysql_get_tableData_of_one_table(self, tableName_or_sqlStatement="子子类赋值的这个表名", enum=0):
        '''
        :param tableName_or_sqlStatement；参数含义：一个数据表名或一条sql查询语句；数据类型为：str；会给个默认值；
        :param enum；参数含义：枚举值；目前枚举值只设置3个；会给个默认值；

        :return 返回值是一个是【元祖内嵌套数组】的查询结果合集；数据类型为元祖（元祖的每个下标对应一个数组，一个数组代表数据表的一条表数据，一条表数据内有包含各表字段的字段值）
        1.该接口作用：执行查询sql语句，返回一个表的符合条件的数据；
        classTest.用于参考的接口返回值： ((35, '这是banner标题', '这是简介内容', '1'),(36, '这是banner标题', '这是简介内容', 'classTest'))
        3.细节：这个接口实现了3个功能，提高了接口复用率(间接证明了自己代码能力有所提高啦)；
        '''
        conn = self.__getConnect()
        cur = conn.cursor()
        try:
            if tableName_or_sqlStatement == "子子类赋值的这个表名" and enum == 0:
                sql = "select * from {tableName}".format(tableName=self.tableName)
            elif tableName_or_sqlStatement != "子子类赋值的这个表名" and enum == 1:
                sql = "select * from {tableName}".format(tableName=tableName_or_sqlStatement)
            elif tableName_or_sqlStatement != "子子类赋值的这个表名" and enum == 2:
                sql = tableName_or_sqlStatement
            else:
                raise Exception("入参值填写有误，请按照要求填写正确的tableName_or_sqlStatement和enum值！")

            cur.execute(sql)
        except Exception as errorMessage:
            print("数据库连接异常,异常信息为：{errorMessage}".format(errorMessage=errorMessage))
        else:
            rows = cur.fetchall()
            cur.close()
            conn.close()
            return rows

    def mysql_get_minimal_value_of_id(self, tableName="子子类赋值的这个表名", enum=0):
        '''
        :param tableName；参数含义：一个数据表名；数据类型为：str；会给个默认值；
        :param enum；参数含义：枚举值；目前枚举值只设置2个；会给个默认值；
        :return 返回一个数据表中最小的id值/最早创建的这条表数据的id值；数据类型为int；
        1.细节：这个接口实现了2个功能；
        '''
        conn = self.__getConnect()
        cur = conn.cursor()
        try:
            if tableName == "子子类赋值的这个表名" and enum == 0:
                sql = "select min(id) from {tableName}".format(tableName=self.tableName)
            elif tableName != "子子类赋值的这个表名" and enum == 1:
                sql = "select min(id) from {tableName}".format(tableName=tableName)
            else:
                raise Exception("入参值填写有误，请按照要求填写正确的tableName和enum值！")
            cur.execute(sql)
        except Exception as errorMessage:
            print("数据库连接异常,异常信息为：{errorMessage}".format(errorMessage=errorMessage))
        else:
            rows = cur.fetchall()
            cur.close()
            conn.close()
            return rows[0][0]

    def mysql_get_maximal_value_of_id(self, tableName="子子类赋值的这个表名", enum=0):
        '''
        :param tableName；参数含义：一个数据表名；数据类型为：str；会给个默认值；
        :param enum；参数含义：枚举值；目前枚举值只设置2个；会给个默认值；
        :return 返回一个数据表中最大的id值/最晚创建的这条表数据的id值；数据类型为int；
        1.细节：这个接口实现了2个功能；
        '''
        conn = self.__getConnect()
        cur = conn.cursor()
        try:
            if tableName == "子子类赋值的这个表名" and enum == 0:
                sql = "select max(id) from {tableName}".format(tableName=self.tableName)
            elif tableName != "子子类赋值的这个表名" and enum == 1:
                sql = "select max(id) from {tableName}".format(tableName=tableName)
            else:
                raise Exception("入参值填写有误，请按照要求填写正确的tableName和enum值！")
            cur.execute(sql)
        except Exception as errorMessage:
            print("数据库连接异常,异常信息为：{errorMessage}".format(errorMessage=errorMessage))
        else:
            rows = cur.fetchall()
            cur.close()
            conn.close()
            return rows[0][0]

    def mysql_get_count_of_one_table_data(self, tableName="子子类赋值的这个表名", enum=0):

        '''
        :param tableName；参数含义：一个数据表名；数据类型为：str；会给个默认值；
        :param enum；参数含义：枚举值；目前枚举值只设置2个；会给个默认值；
        :return 返回一张数据表的表数据的总数 ,数据类型为int；
        1.细节：这个接口实现了2个功能；
        '''
        conn = self.__getConnect()
        cur = conn.cursor()
        try:
            if tableName == "子子类赋值的这个表名" and enum == 0:
                sql = "select count(id) from  {tableName}".format(tableName=self.tableName)
            elif tableName != "子子类赋值的这个表名" and enum == 1:
                sql = "select count(id) from  {tableName}".format(tableName=tableName)
            else:
                raise Exception("入参值填写有误，请按照要求填写正确的tableName和enum值！")
            cur.execute(sql)
        except Exception as errorMessage:
            print("数据库连接异常,异常信息为：{errorMessage}".format(errorMessage=errorMessage))
        else:
            rows = cur.fetchall()
            cur.close()
            conn.close()
            return rows[0][0]

    def mysql_close(self):
        '''
        1.该接口作用：返回一个mysql数据库连接；
        '''
        conn = self.__getConnect()
        try:
            conn.close()
        except Exception as errorMessage:
            print("数据库关闭异常,异常信息为：{errorMessage}".format(errorMessage=errorMessage))

    def mysql_truncate_data(self, oneTableName_or_manyTableNames="子子类赋值的这个表名", enum=0):

        '''
        :param oneTableName_or_manyTableNames；参数含义：一个数据表名或多个数据表名集合；数据类型为：str或者list；会给个默认值；
        :param enum；参数含义：枚举值；目前枚举值只设置3个；会给个默认值；
        1.接口作用： 清空一至多个表数据后，让自增id仍从1开始，数据表仍保留；
        classTest.测试数据demo，比如： tableNames = ["tp_banner","tp_cases","tp_books"]；
        '''
        if oneTableName_or_manyTableNames == "子子类赋值的这个表名" and enum == 0:
            sql = "truncate {tableName}".format(tableName=self.tableName)
            self.mysql_execute(sql)
        elif oneTableName_or_manyTableNames != "子子类赋值的这个表名" and enum == 1:
            if isinstance(oneTableName_or_manyTableNames, str):
                sql = "truncate {tableName}".format(tableName=oneTableName_or_manyTableNames)
                self.mysql_execute(sql)
        elif oneTableName_or_manyTableNames != "子子类赋值的这个表名" and enum == 2:
            if isinstance(oneTableName_or_manyTableNames, list):
                for i in oneTableName_or_manyTableNames:
                    sql = "truncate {tableName}".format(tableName=i)
                    self.mysql_execute(sql)

        elif oneTableName_or_manyTableNames is not None:
            sql = "truncate {tableName}".format(tableName=oneTableName_or_manyTableNames)
            self.mysql_execute(sql)

        else:
            raise Exception("入参值填写有误，请按照要求填写正确的tableName_or_sqlStatement和enum值！")

    # ====================================================这些接口待改造================================================================================================

    def mysql_backups_data_of_tables(self, tableNames: list):
        '''
        :param tableNames； 参数含义：一至多个数据表的集合；数据类型为list；
        1.接口作用：生成一至多个数据表的备份，留当做每次执行一遍所有接口用例后生成的所有表数据的存档，跟开发人员讲相关bug时当证据；（删除/清空原始表数据之前，最好备份出一份新表当证据；）
        classTest.细节：有个备份数据表并重命名新备份表的sql语句，这个sql和相关代码后续再写,可咨询开发（新备份表的表名建议以精准到秒的时间当后缀名，例如：tp_banner20181129201020）
        3.备注：这个接口先暂时不实现功能；
        '''
        pass

    def cancelOneData(self, data):
        '''
        :param data  从excel文件获取到的单条接口数据，数据类型为dict
        接口作用： 删除某条数据表中已存在的数据(为了不干扰【更新接口】的接口测试用例的准确性)
        classTest.注意事项： 如果有其他新项目/新模块的接口跟这个接口内部逻辑不一样，可以在子类继承并重写该方法内的相关逻辑
        数字门店商品上下架.编写以下的范例脚本，不需调试，仅供参考，不用于其他途径
        :return None
        '''
        if data["表字段名"] == "字段名对应的具体值":
            self.mysql_execute("DELETE FROM 单个表名 WHERE 字段名 = 字段名对应的具体值")
        elif data["sort"] == 1:
            self.mysql_execute("DELETE FROM tp_banner WHERE sort = 造第二组仓库相关数据")
        elif data["sort"] == 99998:
            self.mysql_execute("DELETE FROM tp_banner WHERE sort = 99998")
        elif data["sort"] == 99999:
            self.mysql_execute("DELETE FROM tp_banner WHERE sort = 99999")
        elif data["sort"] == 100000:
            self.mysql_execute("DELETE FROM tp_banner WHERE sort = 100000")
        elif data["sort"] == -3:
            self.mysql_execute("DELETE FROM tp_banner WHERE sort = -数字门店商品上下架")
        elif data["sort"] == -6.6:
            self.mysql_execute("DELETE FROM tp_banner WHERE sort = -6.6")
        elif data["sort"] == 2.2:
            self.mysql_execute("DELETE FROM tp_banner WHERE sort = classTest.classTest")

    # ==================================================================================================================================================================

    #  ===以下接口都是为了实现新功能写的新接口，都调试通过；(从2020.04.04星期六开始写)
    # 注意：旧接口就保留原状，不要去动旧接口，因为旧接口可能被多个地方使用了；旧接口的改造或删除可以等后期有时间再处理；
    # 注意：如果要实现新功能，按照开发规范的正确处理方式，可以在旧接口里面改造，也可以直接写个新接口；

    def get_oneRandomValue_or_allValues_of_one_tableField(self, a_designated_tableField_name: str,
                                                          specified_query_criteria=list, enum=0):
        '''
       :param tableName；参数含义：一个数据表名；数据类型为：str；
       :param a_designated_tableField_name 参数名：一个指定的表字段名；数据类型：str；
       :param specified_query_criteria 参数名：指定的查询条件；数据类型：list；比如值为：[["name","Tom"],["age",20]]；
                                       查询条件里支持嵌套零至多组list；每个list里的下标0对应下标值是一个表字段名A，下标1对应下标值是一个表字段名A的表字段值；
                                       目前最多嵌套4组list，后续可再优化；
       :param enum；参数含义：枚举值；目前枚举值只设置2个；会给个默认值0；
       :return
                enum枚举值为0表示"随机返回符合查询条件的一个默认数据表的一个表字段的一个值，值数据类型不固定"；
                enum枚举值为1表示"返回符合查询条件的一个默认数据表的一个表字段的所有值，值数据类型为list" ；
       '''

        conn = self.__conn()
        cur = conn.cursor()

        try:
            if len(specified_query_criteria) == 0:
                sql = "select %s from %s" % (a_designated_tableField_name, self.tableName)
            elif len(specified_query_criteria) == 1:
                sql = "select %s from %s where %s = '%s'" % (
                a_designated_tableField_name, self.tableName, specified_query_criteria[0][0],
                specified_query_criteria[0][1])
            elif len(specified_query_criteria) == 2:
                sql = "select %s from %s where %s ='%s' and %s = '%s'" % (
                a_designated_tableField_name, self.tableName, specified_query_criteria[0][0],
                specified_query_criteria[0][1], specified_query_criteria[1][0], specified_query_criteria[1][1])
            elif len(specified_query_criteria) == 3:
                sql = "select %s from %s where %s = '%s' and %s = '%s' and %s = '%s'" % (
                a_designated_tableField_name, self.tableName, specified_query_criteria[0][0],
                specified_query_criteria[0][1], specified_query_criteria[1][0], specified_query_criteria[1][1],
                specified_query_criteria[2][0], specified_query_criteria[2][1])
            elif len(specified_query_criteria) == 4:
                sql = "select %s from %s where %s = '%s' and %s ='%s' and %s = '%s' and %s = '%s'" % (
                a_designated_tableField_name, self.tableName, specified_query_criteria[0][0],
                specified_query_criteria[0][1], specified_query_criteria[1][0], specified_query_criteria[1][1],
                specified_query_criteria[2][0], specified_query_criteria[2][1], specified_query_criteria[3][0],
                specified_query_criteria[3][1])
            # print(sql)
            cur.execute(sql)

        except Exception as a:
            print("执行SQL语句出现异常：%s" % a)
        else:
            rows = cur.fetchall()
            cur.close()
            conn.close()
            # print(len(rows))
            # print(rows)
            emptyList = []
            for i in rows:
                emptyList.append(i[0])
            # print("111")
            # print(emptyList)
            # print("222")
            if enum == 0:
                value = random.choice(emptyList)
                return value
            elif enum == 1:
                value = emptyList
                return value


if __name__ == "__main__":
    # 单元测试通过
    from douban.settings import mysql_database_connection_information

    test = MysqlUtil()

    test.mysql_database_connection_information = mysql_database_connection_information

    test.tableName = "test1"  # 给属性tableName赋值
    a = test.mysql_get_maximal_value_of_id()
    print(a)

    item = {'describe': '海豚的微笑，是世界上最高明的伪装。',

            'evaluate': '290085人评价',
            # 'introduce': ["导演:路易·西霍尤斯LouiePsihoyos主演:RichardO'Barry/路易·西霍...",
            #               '2009/美国/纪录片'],
            'introduce': ["导演:路易·西霍尤斯LouiePsihoyos主演:RichardO",
                          '2009/美国/纪录片'],
            'movie_name': '海豚湾',
            'serial_number': '73',
            'star': '9.3'}
    serial_number = item['serial_number']
    movie_name = item['movie_name']
    print(movie_name)
    introduce = item['introduce']
    print(introduce)
    star = item['star']
    evaluate = item['evaluate']
    describe1 = item['describe']

    sql = r'insert into test1(serial_number,movie_name,introduce,star,evaluate,describe1) VALUES ("{}","{}","{}","{}","{}","{}")'.format(
        serial_number, movie_name, introduce, star, evaluate, describe1)
    test.mysql_execute(sql)
