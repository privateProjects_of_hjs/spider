import scrapy
import json
from ..items import ChinamovieOfDoubanItem

class ChinamovieOfDoubanSpiderSpider(scrapy.Spider):
    name = 'chinaMovie_of_douban_spider'
    allowed_domains = ['movie.douban.com']
    start_urls = ['https://movie.douban.com/j/search_subjects?type=movie&tag=华语&sort=recommend&page_limit=20&page_start=0']
    base_page = 1 # 设置一个当参考的第几页，这边值为1表示是第一页；
    def parse(self, response):
        movie_text = response.text
        movie_dict = json.loads(movie_text)

        for one_movie in movie_dict["subjects"]:
            item = ChinamovieOfDoubanItem()  # item对象必须在for循环里面，有经过调试和测试了！因为每次for循环生成的item对象都是不一样的内存地址，推送给pipelines管道的每一次item对象里的一条具体dict类型的值都是唯一的；
            title = one_movie["title"]
            rate = one_movie["rate"]
            item["title"] = title
            item["rate"] = rate
            detail_url = one_movie["url"]
            yield scrapy.Request(url=detail_url,meta={"item":item},callback=self.detail_parse)
        # 下一页url地址
        url_of_next_page = 'https://movie.douban.com/j/search_subjects?type=movie&tag=华语&sort=recommend&page_limit=20&page_start=%s'%str(self.base_page*20)  # 要找到接口入参特定入参的入参值变化
        self.base_page = self.base_page +1
        print(self.base_page)
        if self.base_page <=30: # 如果到第30页了，就停止发送请求；（因为通过实际浏览器页面查看数据，总数据量不超过600个，而每页20个，所以目前项目版本里设置这个条件即可）
            yield scrapy.Request(url=url_of_next_page,callback=self.parse)

    def detail_parse(self, response):
        director = response.xpath('//div[@class="subject clearfix"]/div[@id="info"]/span[1]/span[2]/a/text()').getall() # 后续再对字符串里的值进行处理，目前只是为了学习，暂不处理；
        actor = response.xpath('//div[@class="subject clearfix"]/div[@id="info"]/span[@class="actor"]/span[@class="attrs"]//text()').getall()# 后续再对字符串里的值进行处理，目前只是为了学习，暂不处理；
        item = response.meta["item"]
        item["director"] = director
        item["actor"] = actor
        yield item
