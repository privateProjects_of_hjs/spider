# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class ToutiaoItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()

    title = scrapy.Field()        # 1.新闻标题
    source = scrapy.Field()       # 2.新闻来源
    commentNum = scrapy.Field()   # 3.评论数



