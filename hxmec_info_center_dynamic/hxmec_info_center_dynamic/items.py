# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class HxmecInfoCenterDynamicItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()

    # 一、封面图地址
    cover_map_address = scrapy.Field()
    # 二、标题 title
    title = scrapy.Field()
    # 三、简介 introduction
    introduction = scrapy.Field()
    # 四、发布时间  release_time
    release_time = scrapy.Field()
    # 五、详情  details
    details = scrapy.Field()
