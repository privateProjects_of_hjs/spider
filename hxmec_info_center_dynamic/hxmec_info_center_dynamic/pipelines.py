# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
from .settings import mysql_database_connection_information
from  .utils.mysqlUtil import MysqlUtil
mysqlUtil = MysqlUtil()

class HxmecInfoCenterDynamicPipeline:

    def __init__(self):
        self.connect = MysqlUtil()
        self.connect.mysql_database_connection_information = mysql_database_connection_information
        self.connect.mysql_truncate_data("center_dynamic",0)


    def process_item(self, item, spider):
        # 一、封面图地址
        cover_map_address = item["cover_map_address"]
        # 二、标题 title
        title = item["title"]
        # 三、简介 introduction
        introduction = item["introduction"]
        # 四、发布时间  release_time
        release_time = item["release_time"]
        # 五、详情  details
        details = item["details"]


        sql = 'insert into center_dynamic(cover_map_address,title,introduction,release_time,details) VALUES ("{}","{}","{}","{}","{}")'.format(cover_map_address,title,introduction,release_time,details)
        self.connect.mysql_execute(sql)


        return item