import scrapy
from ..items import HxmecInfoCenterDynamicItem
import re
class HxmecInfoCenterDynamicSpiderSpider(scrapy.Spider):
    name = 'hxmec_info_center_dynamic_spider'
    allowed_domains = ['hxmec.com']
    start_urls = ['http://www.hxmec.com/info-center-dynamic.html']
    base_url = 'http://www.hxmec.com'  # 用于做拼接
    pageNumber = 1  # 页数起始值为1，表示第一页；


    def parse(self, response):

        dynamics = response.xpath('//div[@class="info-center"]/div[@class="info-container"]//div[@class="info-list"]')
        for dynamic in dynamics:
            # 每次生成的item对象都是唯一的，每个对象都会被分配一个唯一的内存地址；
            # item对象必须在for循环体内；
            item = HxmecInfoCenterDynamicItem()
            # 1.封面图地址
            cover_map_address = dynamic.xpath('./a/@style').get()
            cover_map_address = self.base_url + re.findall('"(.+?)"',cover_map_address)[0]
            # 2.标题
            title = dynamic.xpath('.//div[@class="info-title"]/a/text()').get()
            title = title.replace('"','“',1000)
            # 3、简介
            introduction1 = dynamic.xpath('.//div[@class="info-text"]/text()').get()
            introduction = ''.join(introduction1.split())
            introduction = introduction.replace('"','“',1000)
            # 文章的详情地址
            detail_url = dynamic.xpath("./a/@href").get().split("//")[1]
            detail_url = "http://" + detail_url

            item["cover_map_address"] = cover_map_address
            item["title"] = title
            item["introduction"] = introduction
            yield scrapy.Request(url = detail_url , meta = {"item":item}, callback = self.detail)
        # 下一页地址
        self.pageNumber = self.pageNumber + 1
        if self.pageNumber <= 20: # 目前最多只有5页，这边设置为最多只获取前20页数据
            url_of_next = self.start_urls[0] + "?pageIndex=" + str(self.pageNumber)
            yield scrapy.Request(url=url_of_next,callback=self.parse)



    def detail(self,response):
        # 每次请求生成的response对象都是唯一的，每个对象都会被分配一个唯一的内存地址
        item = response.meta["item"]
        # 4.发布时间
        release_time = response.xpath('//div[@class="article-time"]/span[2]/text()').get()
        # 5.详情
        lists = response.xpath('//div[@class="article-text"]//text()').getall()
        details = ""
        for i in lists:
            details = details + i + "\n"
            details = details.replace('"','“',1000)

        item["release_time"] = release_time
        item["details"] = details

        yield item





